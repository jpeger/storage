//
//  ViewController.m
//  Storage
//
//  Created by Stephen Bromley on 1/26/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString* path = [[NSBundle mainBundle]pathForResource:@"PropertyList" ofType:@"plist"];
    array = [[NSArray alloc]initWithContentsOfFile:path];
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    
    if (array.count > 0) {
        [ud setObject:array forKey:@"array"];
        [ud synchronize];
    } else {
        array = [ud objectForKey:@"array"];
    }
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height-20) style:UITableViewStylePlain];
    [myTableView setDelegate:self];
    [myTableView setDataSource:self];
    [self.view addSubview:myTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString* identifier = @"cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    NSDictionary* dictionary = [array objectAtIndex:indexPath.row];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, cell.frame.size.height - 10, cell.frame.size.height - 10)];
    iv.backgroundColor = [UIColor brownColor];
    iv.image = [UIImage imageNamed:dictionary[@"recipeImage"]];
    [cell addSubview:iv];
    
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(iv.frame.origin.x + iv.frame.size.width + 10, 0, cell.frame.size.width - iv.frame.origin.x - iv.frame.size.width - 10, cell.frame.size.height)];
    [lbl setText:dictionary[@"recipeName"]];
    [cell addSubview:lbl];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RecipeViewController* pivc = [[RecipeViewController alloc]init];
    pivc.recipeInfo = [array objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:pivc animated:YES];
    
    
}

@end
