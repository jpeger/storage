//
//  RecipeViewController.m
//  Storage
//
//  Created by jpeger on 2/2/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UILabel* recipeName = [[UILabel alloc]initWithFrame:CGRectMake(10, 65, self.view.frame.size.width-20, 50)];
    [recipeName setText:self.recipeInfo[@"recipeName"]];
    [recipeName setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:recipeName];
    
    UIImageView* recipeImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, recipeName.frame.origin.y+recipeName.frame.size.height, self.view.frame.size.width-20, 100)];
    [recipeImage setImage:[UIImage imageNamed:self.recipeInfo[@"recipeImage"]]];
    [recipeImage setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:recipeImage];
    
    UIScrollView* scrollRecip = [[UIScrollView alloc]initWithFrame:CGRectMake(0, recipeImage.frame.origin.y+recipeImage.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-(recipeImage.frame.size.height+recipeName.frame.size.height+65))];
    [scrollRecip setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height*3)];
    
    [self.view addSubview:scrollRecip];
    
    UITextView* recipeDescription = [[UITextView alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 1000)];
    [recipeDescription setText:self.recipeInfo[@"recipeDescription"]];
    [recipeDescription setUserInteractionEnabled:NO];
    [recipeDescription sizeToFit];
    
    [scrollRecip addSubview:recipeDescription];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
