//
//  ViewController.h
//  Storage
//
//  Created by Stephen Bromley on 1/26/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipeViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    NSArray* array;
}

@end

