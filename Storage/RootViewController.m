//
//  RootViewController.m
//  Storage
//
//  Created by jpeger on 2/2/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    ViewController* mainView = [ViewController new];
    
    UINavigationController* navCon = [[UINavigationController alloc]initWithRootViewController:mainView];
    [self addChildViewController:navCon];
    [self.view addSubview:navCon.view];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
