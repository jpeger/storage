//
//  RecipeViewController.h
//  Storage
//
//  Created by jpeger on 2/2/15.
//  Copyright (c) 2015 Stephen Bromley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeViewController : UIViewController

@property (nonatomic, strong) NSDictionary* recipeInfo;

@end
